import { Route, Routes, useNavigate } from 'react-router'
import { BrowserRouter } from 'react-router-dom'
import HomeDoctor from './pages/guestPages/HomeAppointment/HomeDoctor'
import HomeTemplates from './templates/HomeTemplates'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='home' element={<HomeTemplates />}>
          <Route path='doctor' element={<HomeDoctor />} />
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export default App
