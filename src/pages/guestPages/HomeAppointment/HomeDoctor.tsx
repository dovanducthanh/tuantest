import React from 'react'
import Search from './Search'
import '../../../assets/pages/guestPages/home_appointment.css'
import FilterDoctor from './FilterDoctor'
import InformationAppointment from './InformationAppointment'
import Grid from '@mui/material/Grid'

type Props = {}

export default function HomeDoctor({}: Props) {
  return (
    <section className='container__home-doctor'>
      <h1 className='title'>Search Doctor, Make an Appointment</h1>
      <Search />
      {/* <hr className='line'></hr> */}
      <section className='infor__appointment__container'>
        <Grid container spacing={2} className='grid__container'>
          <Grid item xs={3} className='grid__filter'>
            <FilterDoctor />
          </Grid>
          <Grid item xs={9}>
            <InformationAppointment />
            <InformationAppointment />
            <InformationAppointment />
            <InformationAppointment />
          </Grid>
        </Grid>
      </section>
    </section>
  )
}
