import React from 'react'
import TextField from '@mui/material/TextField'
import MenuItem from '@mui/material/MenuItem'
import { Select } from '@mui/material'

type Props = {}

export default function FilterDoctor({}: Props) {
  return (
    <div className='filter__container'>
      <form action=''>
        <div className='select__form-object'>
          <label htmlFor='select__object' className='label__title'>
            Object
          </label>
          <Select MenuProps={{ disableScrollLock: true }} id='select__object' className='select__text__field'>
            <MenuItem key='Male' value='Male'>
              Male
            </MenuItem>

            <MenuItem key='Female' value='Female'>
              Female
            </MenuItem>

            <MenuItem key='Child' value='Child'>
              Child
            </MenuItem>

            <MenuItem key='Oldster' value='Oldster'>
              Oldster
            </MenuItem>
          </Select>
        </div>

        <div className='select__form-department'>
          <p className='label__title'>Department</p>
          <div className='container__checkbox'>
            <div className='input__checkbox'>
              <input type='checkbox' value='Musculoskeletal' id='Musculoskeletal' />
              <label htmlFor='Musculoskeletal'>Musculoskeletal</label>
            </div>
            <div className='input__checkbox'>
              <input type='checkbox' value='Gastroenterology' id='Gastroenterology' />
              <label htmlFor='Gastroenterology'>Gastroenterology</label>
            </div>
            <div className='input__checkbox'>
              <input type='checkbox' value='Cardiology' id='Cardiology' />
              <label htmlFor='Cardiology'>Cardiology</label>
            </div>
          </div>
        </div>
      </form>
    </div>
  )
}
