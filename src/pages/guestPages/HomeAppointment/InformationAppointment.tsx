import React from 'react'
import ShortDescriptionDoctor from './ShortDescriptionDoctor'
import TimeSlots from './TimeSlots'
import Grid from '@mui/material/Grid'

type Props = {}

export default function InformationAppointment({}: Props) {
  return (
    <div style={{ margin: '10px 0 36px 10px' }}>
      <Grid container spacing={2} className='container__information-appointment'>
        <Grid item xs={6} sx={{ borderRight: '1px solid #ccc' }}>
          <ShortDescriptionDoctor />
        </Grid>
        <Grid item xs={6}>
          <TimeSlots />
        </Grid>
      </Grid>
    </div>
  )
}
