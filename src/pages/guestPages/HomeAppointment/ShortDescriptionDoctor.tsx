import React from 'react'
import { NavLink } from 'react-router-dom'

type Props = {}

export default function ShortDescriptionDoctor({}: Props) {
  return (
    <>
      <div className='container__description'>
        <div className=''>
          <img
            className='img__doctor'
            src='https://cdn.bookingcare.vn/fr/w200/2021/01/18/105401-bsckii-tran-minh-khuyen.jpg'
            alt=''
          />
          <NavLink className='link__see-more' to='/home/doctor/detail/1'>
            See Detail
          </NavLink>
        </div>
        <div className='container__infor'>
          <NavLink to='/home/doctor/detail/1' className='title__doctor'>Bác sĩ Chuyên khoa II Trần Minh Khuyên</NavLink>
          <span className='text_short'>
            Nguyên Trưởng khoa lâm sàng, Bệnh tâm thần Thành phố Hồ Chí Minh Tốt nghiệp Tâm lý trị liệu, trường Tâm lý
            Thực hành Paris (Psychology practique de Paris) Bác sĩ nhận khám từ 16 tuổi trở lên
          </span>
        </div>
      </div>
    </>
  )
}
