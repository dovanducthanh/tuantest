import React from 'react'
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth'
import { NavLink } from 'react-router-dom'
import PanToolAltOutlinedIcon from '@mui/icons-material/PanToolAltOutlined'

type Props = {}

export default function TimeSlots({}: Props) {
  return (
    <div className='time__slot-box'>
      <div style={{ borderBottom: '1px solid #ccc' }}>Friday-1/3</div>
      <div className='package__schedule'>
        <div className='container__icon-time'>
          <CalendarMonthIcon />
          <b>PACKAGE SCHEDULE</b>
        </div>
        <div className='container__time-slots'>
          <NavLink to='/form_book'>
            <span className='time__slot-item'>08:00 - 08:30</span>
          </NavLink>
          <NavLink to='/form_book'>
            <span className='time__slot-item'>08:00 - 08:30</span>
          </NavLink>
          <NavLink to='/form_book'>
            <span className='time__slot-item'>08:00 - 08:30</span>
          </NavLink>
          <NavLink to='/form_book'>
            <span className='time__slot-item'>08:00 - 08:30</span>
          </NavLink>
          <NavLink to='/form_book'>
            <span className='time__slot-item'>08:00 - 08:30</span>
          </NavLink>
          <NavLink to='/form_book'>
            <span className='time__slot-item'>08:00 - 08:30</span>
          </NavLink>
          <NavLink to='/form_book'>
            <span className='time__slot-item'>08:00 - 08:30</span>
          </NavLink>
          <NavLink to='/form_book'>
            <span className='time__slot-item'>08:00 - 08:30</span>
          </NavLink>
        </div>
        <div>
          <span>
            Select <PanToolAltOutlinedIcon />
            and set
          </span>
        </div>
      </div>
      <div className='container__price'>
        <b>PRICE: </b>
        <span>200.000</span>
      </div>

      <div className='container__insurance'>
        <b>TYPE OF INSURANCE APPLICATION: </b>
      </div>
    </div>
  )
}
