import React from 'react'
import Paper from '@mui/material/Paper'
import InputBase from '@mui/material/InputBase'
import Divider from '@mui/material/Divider'
import IconButton from '@mui/material/IconButton'
import MenuIcon from '@mui/icons-material/Menu'
import SearchIcon from '@mui/icons-material/Search'
import DirectionsIcon from '@mui/icons-material/Directions'

type Props = {}

export default function Search({}: Props) {
  return (
    <>
      <Paper component='form' className='form__search'>
        <InputBase sx={{ ml: 1, flex: 1 }} placeholder='Search ' inputProps={{ 'aria-label': 'search ' }} />
        <IconButton type='button' sx={{ p: '10px' }} aria-label='search'>
          <SearchIcon />
        </IconButton>
      </Paper>
    </>
  )
}
