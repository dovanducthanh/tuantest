import React from 'react'
import { Outlet } from 'react-router-dom'

type Props = {}

export default function HomeTemplates({}: Props) {
  return (
    <div style={{ padding: '0px 40px' }}>
      <header>header</header>
      <Outlet />
      <footer></footer>
    </div>
  )
}
